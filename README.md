`<ReactBridOutstreamPlayer>` is a React Component for initializing client-side instances of a Brid Outstream player. Simply give `<ReactBridOutstreamPlayer>` the id of your player, provide the unit type, the ID of the HTML element in which you want the player to initialize, and a couple of other configuration parameters and you are good to go. The component comes with several event hooks that can be accessed through component props.

## Contents

- [Installation](#installation)
- [Usage](#usage)
- Props
  - [Required Props](#required-props)
  - Optional Props
    - [Event Hooks](#event-hooks)
      - [Advertising](#optional-advertising-event-hook-props)
      - [Player Events](#optional-player-event-hook-props)
- [Example Container Component](#example-container-component)
- [Contributing](#contributing)

## Installation

```shell
npm install react-brid-outstream-player
```

## Usage

At the mininum, you can just use something like the below code snippet:

### Displaying a Brid Outstream Player

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import ReactBridOutstreamPlayer from 'react-brid-outstream-player';

ReactDOM.render(
  <ReactBridOutstreamPlayer
    type="incontent"
    divId="Brid_12345"
    id="5817"
    width="640"
    height="360"
  />,
  document.getElementById('my-root-div');
);
```

For more complex interaction, check out the container component example [here](#example-container-component)

## Required Props

These are props that modify the basic behavior of the component.

- `type`
  - The type of outstream unit you plan on using. Acceptable values are "incontent" and "inslide".
  - Type: `string`
  - Example: `type="incontent"`
- `divId`
  - The ID of the DIV in which the unit will render in. This ID is useful to have to use the player's JS API for additional manipulation.
  - Type: `string`
  - Example: `divId="brid_12345"`
- `id`
  - The ID of the unit you want to use. To get this ID for your account please see this entry [here](https://developer.brid.tv/brid-player/javascript-api-reference)
  - Type: `string`
  - Example: `id="12345"`
- `width`
  - Set the width in pixels of your ad unit. Also accepted values here are percentages like 100%.
  - Type: `string`
  - Example: `width="640"`
- `height`
  - Set the height in pixels of your ad unit. Also accepted values here are percentages like 100%.
  - Type: `string`
  - Example: `height="100%"`

# Event Hooks

`react-brid-outstream-player` dynamically supports all events in Brid Player. Simply preface the event name with `on` and pass it in as a prop.

Examples:

- `requestAd` => `onRequestAd`
- `adStart` => `onAdStart`

`react-brid-outstream-player` has layered some different functionality on some of these events, so please check the docs below if you find any unexpected behavior!

`playerReady` is present as a special prop which is called by the player whenever the player is ready for API manipulation.

## Optional Advertising Event Hook Props

- `onRequestAd(event)`
  - A function that is run when the player calls your ad tag URL.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdImpression(event)`
  - A function that is run when the player fires your ads impression pixel.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdFirstQuartile(event)`
  - A function that is run when the user sees 25% of your advertisement.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdMidpoint(event)`
  - A function that is run when the user sees 50% of your advertisement.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdThirdQuartile(event)`
  - A function that is run when the user sees 75% of your advertisement.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdEnd(event)`
  - A function that is run when the user sees 100% of your advertisement.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdPause(event)`
  - A function that is run when the user pauses the preroll advertisement.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdStart(event)`
  - A function that is run once, when the preroll advertisement first starts to play.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdResume(event)`
  - A function that is run when the user resumes playing the preroll advertisement.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdError(event)`
  - A function that is run when your advertisement errors out for any reason. This includes empty VAST/VPAID respones.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.

## Optional Player Event Hook Props

- `onFullScreen(event)`
  - A function that is run when a user clicks on the player's fullscreen button.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onMuteChange(event)`
  - A function that is run when a user changes the sound state of the player.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.

## Example Container Component

```javascript
import React from "react";
import ReactBridOutstreamPlayer from "react-brid-outstream-player";

const displayName = "ReactBridOutstreamContainer";

class ReactBridOutstreamContainer extends React.Component {
  constructor(props) {
    super(props);

    this.onAdStart = this.onAdStart.bind(this);
    this.onAdImpression = this.onAdImpression.bind(this);

    this.divId = someFunctionToRandomlyGenerateId();
  }
  onAdStart(event) {
    // Do some custom code here once an ad starts playback
  }
  onAdImpression(event) {
    // Track ad impressions here
  }

  render() {
    return (
      <div className="react-brid-outstream-container">
        <ReactBridOutstreamPlayer
          type="incontent"
          divId={this.divId}
          id="5817"
          width="640"
          height="360"
          onAdStart={this.onAdStart}
          onAdImpression={this.onAdImpression}
        />
      </div>
    );
  }
}

ReactBridOutstreamContainer.displayName = displayName;
export default ReactBridOutstreamContainer;
```

## Contributing

First, thank you for taking the time to contribute something to this Brid Outstream react component!
Your help is always welcome! Feel free to open issues, ask questions, talk about it and discuss.

### Pull Requests

When contributing to this repo, please first discuss the change you wish to make via issue, email, or any other method.

### Feature Requests

I am always interested in expanding the feature-set of this component so if there is something you need, just open a ticket [here](https://brid.zendesk.com/hc/en-us/requests/new) or [drop me a line](mailto:support@brid.tv).
