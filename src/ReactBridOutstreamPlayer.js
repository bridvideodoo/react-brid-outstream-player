import React, { Component } from "react";
import InjectPlayerScript from "./helpers/InjectPlayerScript";
import RemoveBridOutstreamInstance from "./helpers/RemoveBridOutstreamInstance";
import AddEventListeners from "./helpers/AddEventListeners";

class ReactBridOutstreamPlayer extends Component {
  constructor(props) {
    super(props);
    this.scriptSrc = "//services.brid.tv/player/build/brid.outstream.min.js";
    this.uniqueScriptId = "brid-outstream-script";
    this.playerConfig = {
      id: this.props.id,
      width: this.props.width,
      height: this.props.height
    };
    this.state = { ready: "false", div: {} };
  }

  componentDidMount() {
    InjectPlayerScript({
      context: document,
      onLoadCallback: this.initialize,
      scriptSrc: this.scriptSrc,
      uniqueScriptId: this.uniqueScriptId
    });
  }

  componentWillUnmount() {
    RemoveBridOutstreamInstance(this.props.divId, window, this.props.type);
  }
  
  initialize = () => {
    const player = window.$bos(this.props.divId, this.playerConfig, this.props.playerReady);
    this.setState({ ready: true, div: player });
    this.AddEventListeners = AddEventListeners(
      this,
      window.$bos,
      this.state.div
    );
  };

  renderOutstream = () => {
    if (this.props.type !== "inslide") {
      return (
        <div
          id={this.props.divId}
          className="brid"
          style={{ width: this.props.width, height: this.props.height }}
        />
      );
    } else return null;
  };

  render() {
    return this.renderOutstream();
  }
}

export default ReactBridOutstreamPlayer;
