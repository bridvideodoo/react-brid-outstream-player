import {
  onRequestAd,
  onAdStart,
  onAdImpression,
  onAdFirstQuartile,
  onAdMidpoint,
  onAdThirdQuartile,
  onAdEnd,
  onAdError,
  onAdPause,
  onAdResume,
  onFullScreen,
  onMuteChange
} from "./EventListeners";

function AddEventListeners(component, config, playerObject) {
  if (component.props.type === "incontent") {
    return {
      onRequestAd: config(component.props.divId).add("requestAd", onRequestAd),
      onAdStart: config(component.props.divId).add("adStart", onAdStart),
      onAdImpression: config(component.props.divId).add("adImpression", onAdImpression),
      onAdFirstQuartile: config(component.props.divId).add("adFirstQuartile", onAdFirstQuartile),
      onAdMidpoint: config(component.props.divId).add("adMidpoint", onAdMidpoint),
      onAdThirdQuartile: config(component.props.divId).add("adThirdQuartile", onAdThirdQuartile),
      onAdEnd: config(component.props.divId).add("adEnd", onAdEnd),
      onAdError: config(component.props.divId).add("adError", onAdError),
      onAdPause: config(component.props.divId).add("adPause", onAdPause),
      onAdResume: config(component.props.divId).add("adResume", onAdResume),
      onFullScreen: config(component.props.divId).add("fullscreenchange", onFullScreen),
      onMuteChange: config(component.props.divId).add("mutechange", onMuteChange)
    };
  } else {
    return {
      onRequestAd: playerObject.add("requestAd", onRequestAd),
      onAdPlay: playerObject.add("adStart", onAdStart),
      onAdImpression: playerObject.add("adImpression", onAdImpression),
      onAdFirstQuartile: playerObject.add("adFirstQuartile", onAdFirstQuartile),
      onAdMidpoint: playerObject.add("adMidpoint", onAdMidpoint),
      onAdThirdQuartile: playerObject.add("adThirdQuartile", onAdThirdQuartile),
      onAdEnd: playerObject.add("adEnd", onAdEnd),
      onAdError: playerObject.add("adError", onAdError),
      onAdPause: playerObject.add("adPause", onAdPause),
      onAdResume: playerObject.add("adResume", onAdResume),
      onFullScreen: playerObject.add("fullscreenchange", onFullScreen),
      onMuteChange: playerObject.add("mutechange", onMuteChange)
    };
  }
}

export default AddEventListeners;
