function onRequestAd(event) {
  this.props.onRequestAd(event);
}
function onAdStart(event) {
  this.props.onAdStart(event);
}
function onAdImpression(event) {
  this.props.onAdImpression(event);
}
function onAdFirstQuartile(event) {
  this.props.onAdFirstQuartile(event);
}
function onAdMidpoint(event) {
  this.props.onAdMidpoint(event);
}
function onAdThirdQuartile(event) {
  this.props.onAdThirdQuartile(event);
}
function onAdEnd(event) {
  this.props.onAdEnd(event);
}
function onAdError(event) {
  this.props.onAdError(event);
}
function onAdPause(event) {
  this.props.onAdPause(event);
}
function onAdResume(event) {
  this.props.onAdResume(event);
}
function onFullScreen(event) {
  this.props.onFullScreen(event);
}
function onMuteChange(event) {
  this.props.onMuteChange(event);
}

export {
  onRequestAd,
  onAdStart,
  onAdImpression,
  onAdFirstQuartile,
  onAdMidpoint,
  onAdThirdQuartile,
  onAdEnd,
  onAdError,
  onAdPause,
  onAdResume,
  onFullScreen,
  onMuteChange
};
