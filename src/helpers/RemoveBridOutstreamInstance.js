function RemoveBridOutstreamInstance(playerId, context, type) {
  const player = context.$bp;
  if (player && type !== "inslide") {
    player(playerId).destroy();
  } else {
    this.destroy();
  }
}

export default RemoveBridOutstreamInstance;
