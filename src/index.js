import React from "react";
import ReactDOM from "react-dom";
import ReactBridOutstreamPlayer from "./ReactBridOutstreamPlayer";

ReactDOM.render(
  <ReactBridOutstreamPlayer
    type="incontent"
    divId="Brid_12345"
    id="5817"
    width="640"
    height="360"
  />,
  document.getElementById("root")
);

if (module.hot) {
  module.hot.accept();
}
